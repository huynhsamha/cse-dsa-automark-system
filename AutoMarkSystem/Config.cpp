#include "Config.h"

const QString Config::ROOT                          =   "./../DSA_DATA";

const QString Config::PATH_SERVER                   =   "SERVER";
const QString Config::PATH_CLIENT                   =   "CLIENT";
const QString Config::PATH_STUDENTS                 =   "STUDENTS";
const QString Config::PATH_TESTCASES                =   "TESTCASES";
const QString Config::PATH_SUBMISSIONS              =   "Submissions";
const QString Config::PATH_OUTPUT                   =   "OUTPUT";

const QString Config::FILE_XML_RECORD               =   "Record.xml";
const QString Config::FILE_TXT_SCORE                =   "Score.txt";
const QString Config::FILE_EXE_MARK                 =   "mark.exe";
const QString Config::FILE_TXT_WAITING              =   "waiting.txt";
const QString Config::FILE_XML_PRO                  =   "pro.xml";
const QString Config::FILE_TXT_LOG                  =   "log.txt";
const QString Config::FILE_EXE_MAIN                 =   "main.exe";

const QString Config::TAG_XML_ASSIGNMENT            =   "assignment";

const QString Config::ATTR_XML_ID                   =   "id";
const QString Config::ATTR_XML_NUMBER_TESTCASES     =   "number_testcases";
const QString Config::ATTR_XML_TIME_LIMIT_EXCEED    =   "time_limit_exceed";
const QString Config::TAG_XML_PROBLEMID             =   "problemID";
const QString Config::TAG_XML_USERID                =   "userID";
const QString Config::TAG_XML_SUBMISSIONID          =   "submissionID";
const QString Config::TAG_XML_PRIORITY              =   "priority";

const int Config::TIME_LIMIT_COMPILE                =   2000;
