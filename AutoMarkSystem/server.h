#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QtXml>
#include <QTextStream>
#include <QProcess>

#include "DataStructures/Tree.h"
#include "DataStructures/PriorityQueue.h"
#include "DataStructures/DoubleLinkedList.h"

#include "Models/student.h"
#include "Models/assignment.h"
#include "Models/submission.h"

#include "Config.h"
#include "utilities.h"

class Server : public QThread
{
    Q_OBJECT
public:
    explicit Server(QMutex *sharedMutex, PriorityQueue<Submission> *sharedQueue);

    // overriding the QThread's run() method
    void run();

private:

    QMutex *mutex; // lấy con trỏ mutex từ hàm main
    PriorityQueue<Submission> *queue; // lấy con trỏ queue từ hàm main

    // các thư mục dùng dùng chính, sử dụng nhiều lần trong project
    QDir DirRoot, DirServer, DirClient;

    // quản lý danh sách sinh viên và bài tập
     DoubleLinkedList<Assignment> *assignmentList;

signals:

public slots:

private:

    void LoadAssignment(); // load các assignment hiện tại của hệ thống.
    void CompileSubmissionSync(Submission submission, Assignment assignment);
    void RunSubmissionSync(Submission submission, Assignment assignment);
    void MarkOutputSubmissionSync(Submission submission, Assignment assignment);
};

#endif // SERVER_H
