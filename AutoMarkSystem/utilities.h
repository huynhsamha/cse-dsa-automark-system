#ifndef UTILITIES_H
#define UTILITIES_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QtXml>
#include <QTextStream>

// các hàm tiện ích của hệ thống
class Utilities
{
public:

    // hàm copy thư mục trong hệ thống
    static bool CopyDirectory(const QString &sourceFilePath, const QString &targetFilePath);

};

#endif // UTILITIES_H
