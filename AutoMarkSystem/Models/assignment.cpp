#include "assignment.h"

Assignment::Assignment(const char *id): ID(id) {}

QString Assignment::getID()
{
    return ID;
}

void Assignment::setID(const QString &value)
{
    ID = value;
}

DoubleLinkedList<Submission> Assignment::getSubmissionList()
{
    return submissionList;
}

int Assignment::getSubmissionCounter()
{
    return submissionList.Size();
}

int Assignment::getNumberTestCases()
{
    return numberTestCases;
}

void Assignment::setNumberTestCases(int value)
{
    numberTestCases = value;
}

QString Assignment::getPath()
{
    return path;
}

void Assignment::setPath(const QString &value)
{
    path = value;
}

void Assignment::addSubmission(Submission o) {
    submissionList.InsertLast(o);
}

Submission Assignment::getSubmission(int index) {
    return submissionList.GetValue(index);
}

int Assignment::getTimeLimitExceed() const
{
    return timeLimitExceed;
}

void Assignment::setTimeLimitExceed(int value)
{
    timeLimitExceed = value;
}
