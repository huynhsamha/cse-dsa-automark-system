#include "student.h"

Student::Student(const char *id): ID(id) {}

QString Student::getID() const
{
    return ID;
}

void Student::setID(const QString &value)
{
    ID = value;
}

DoubleLinkedList<Submission> Student::getSubmissionList()
{
    return submissionList;
}

int Student::getSubmissionCounter()
{
    return submissionList.Size();
}

void Student::addSubmission(Submission o) {
    submissionList.InsertLast(o);
}

Submission Student::getSubmission(int index)  {
    return submissionList.GetValue(index);
}
