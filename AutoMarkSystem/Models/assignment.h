#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include <QString>
#include <QDebug>
#include <QSet>

#include "DataStructures/DoubleLinkedList.h"

#include "Models/submission.h"

class Assignment
{
public:
    Assignment(const char *id);

    QString getID();
    void setID(const QString &value);

    DoubleLinkedList<Submission> getSubmissionList();
    int getSubmissionCounter();

    int getNumberTestCases();
    void setNumberTestCases(int value);

    QString getPath();
    void setPath(const QString &value);

    void addSubmission(Submission o);
    Submission getSubmission(int index);

    int getTimeLimitExceed() const;
    void setTimeLimitExceed(int value);

private:
    QString ID;
    DoubleLinkedList<Submission> submissionList;
    int numberTestCases;
    int timeLimitExceed;
    QString path;
};

#endif // ASSIGNMENT_H
