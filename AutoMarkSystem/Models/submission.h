#ifndef SUBMISSION_H
#define SUBMISSION_H

#include <QString>
#include <QDebug>
#include <QDir>

class Submission
{
public:
    Submission();
    Submission(const char *path);

    bool operator <(const Submission &o);
    bool operator >(const Submission &o);

    QString getAssignmentID() const;
    void setAssignmentID(const QString &value);

    QString getStudentID() const;
    void setStudentID(const QString &value);

    QString getPath() const;
    void setPath(const QString &value);

    int getPriority() const;
    void setPriority(int value);

    int getScore() const;
    void setScore(int value);

    QString getId() const;
    void setId(const QString &value);

private:
    QString id;
    QString assignmentID;
    QString studentID;
    QString path; // đường dẫn tuyệt đối thư mục
    int priority; // độ ưu tiên của submission này
    int score; // điểm của submission
};

Q_DECLARE_METATYPE(Submission)

#endif // SUBMISSION_H
