#ifndef STUDENT_H
#define STUDENT_H

#include <QString>
#include <QDebug>
#include <QSet>

#include "DataStructures/DoubleLinkedList.h"

#include "Models/submission.h"

class Student
{
public:
    Student(const char *id);

    QString getID() const;
    void setID(const QString &value);

    DoubleLinkedList<Submission> getSubmissionList();
    int getSubmissionCounter();
    void addSubmission(Submission o);
    Submission getSubmission(int index);

private:
    QString ID;
    DoubleLinkedList<Submission> submissionList;
};

#endif // STUDENT_H
