#include "submission.h"

bool Submission::operator <(const Submission &o) {
    return priority < o.priority;
}

bool Submission::operator >(const Submission &o) {
    return priority > o.priority;
}

QString Submission::getAssignmentID() const
{
    return assignmentID;
}

void Submission::setAssignmentID(const QString &value)
{
    assignmentID = value;
}

QString Submission::getStudentID() const
{
    return studentID;
}

void Submission::setStudentID(const QString &value)
{
    studentID = value;
}

QString Submission::getPath() const
{
    return path;
}

void Submission::setPath(const QString &value)
{
    path = value;
}

int Submission::getPriority() const
{
    return priority;
}

void Submission::setPriority(int value)
{
    priority = value;
}

int Submission::getScore() const
{
    return score;
}

void Submission::setScore(int value)
{
    score = value;
}

QString Submission::getId() const
{
    return id;
}

void Submission::setId(const QString &value)
{
    id = value;
}

Submission::Submission(): score(-1), priority(0) {}

Submission::Submission(const char *path): Submission() {
    this->path = QString(path);
}
