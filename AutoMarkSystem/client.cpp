#include "client.h"

Client::Client(QMutex *sharedMutex, PriorityQueue<Submission> *sharedQueue)
{
    qDebug() << "===========================  Client is init  ===============================";
    mutex = sharedMutex;
    queue = sharedQueue;

    DirRoot = QDir(Config::ROOT);
    DirServer = DirRoot; DirServer.cd(Config::PATH_SERVER);
    DirClient = DirRoot; DirClient.cd(Config::PATH_CLIENT);

    qDebug() << DirRoot.path();
    qDebug() << DirClient.path();
    qDebug() << DirServer.path();

    watcher = new QFileSystemWatcher(this);

    watcher->addPath(DirClient.absolutePath());

    connect(watcher, SIGNAL(directoryChanged(QString)), this, SLOT(HandleDirectoryChanged(QString)));

    qDebug() << "=========================================================================";
}



void Client::run()
{
}



Client::HandleDirectoryChanged(const QString &path)
{
    qDebug() << "\n\n";
    qDebug() << "=======================  Client  =========================";
    qDebug() << "Directory Client changed: " + path;
    QDir dirChanged(path);
    foreach (QFileInfo item, dirChanged.entryInfoList(QDir::NoDotAndDotDot | QDir::AllDirs)) {
        if (!CheckSubmissionPassed(item.filePath())) {
            if (AddNewSubmissionToQueue(item.filePath())) {
                qDebug() << "Success: New submission is added to queue";
            } else {
                qDebug() << "Error: error submission";
            }
            qDebug() << "==========================================================";
            qDebug() << "\n\n";
        }
    }
}




bool Client::CheckSubmissionPassed(const QString &path)
{
    qDebug() << "CheckSubmissionPassed";
    QDir dir(path);
    QFileInfo fileWaiting(dir.filePath(Config::FILE_TXT_WAITING));
    if (fileWaiting.exists() && fileWaiting.isFile()) {
        qDebug() << "Old submission";
        return true;
    }
    qDebug() << "New submission";
    QFile file(dir.filePath(Config::FILE_TXT_WAITING));
    file.open(QIODevice::ReadWrite); // tạo file waiting
    return false;
}




bool Client::AddNewSubmissionToQueue(const QString &path)
{
    qDebug() << "AddNewSubmissionToQueue";
    QDir dir(path);
    QDir dirAss = dir; dirAss.cdUp();

    Submission submission(path.toStdString().c_str());

    if (!ParseFileXMlToSubmission(submission, path)) {
        qDebug() << "Failed";
        return false;
    }

    qDebug() << "Success";

    QDir dirServer = DirServer;
    dirServer.cd(submission.getAssignmentID());
    dirServer.cd(Config::PATH_STUDENTS);
    if (!dirServer.cd(submission.getStudentID())) {
        dirServer.mkdir(submission.getStudentID());
        dirServer.cd(submission.getStudentID());
    }
    if (!dirServer.cd(Config::PATH_SUBMISSIONS)) {
        dirServer.mkdir(Config::PATH_SUBMISSIONS);
        dirServer.cd(Config::PATH_SUBMISSIONS);
    }
    submission.setPath(dirServer.absoluteFilePath(dir.dirName()));
    Utilities::CopyDirectory(path, submission.getPath());

    mutex->lock();
        queue->push(submission);
    mutex->unlock();

    return true;
}




bool Client::ParseFileXMlToSubmission(Submission &submission, const QString &path)
{
    qDebug() << "ParseFileXMlToSubmission";
    qDebug() << path;
    QDir dir(path);
    qDebug() << dir.absoluteFilePath(Config::FILE_XML_PRO);
    QFileInfo fileInfo(dir.absoluteFilePath(Config::FILE_XML_PRO));

    if (!fileInfo.exists()) {
        qWarning("Not exist file pro.xml");
        return false;
    }

    QFile file(fileInfo.absoluteFilePath());

    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("Can not open file pro.xml");
        return false;
    }

    QDomDocument document;
    if (!document.setContent(&file)) {
        file.close();
        return false;
    }
    file.close();

    QDomElement root = document.documentElement();
    QDomNode node = root.firstChild();
    while (!node.isNull()) {
        QDomElement element = node.toElement();
        QString tagName = element.tagName();
        qDebug() << tagName;
        if (tagName == Config::TAG_XML_PROBLEMID) {
            submission.setAssignmentID(element.text());
        }
        else if (tagName == Config::TAG_XML_USERID) {
            submission.setStudentID(element.text());
        }
        else if (tagName == Config::TAG_XML_SUBMISSIONID) {
            submission.setId(element.text());
        }
        else if (tagName == Config::TAG_XML_PRIORITY) {
            submission.setPriority(element.text().toInt());
        }
        node = node.nextSibling();
    }

    return true;
}
