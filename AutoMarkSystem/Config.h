#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

// file cấu hình của hệ thống
class Config
{
public:
    static const QString ROOT;

    static const QString PATH_SERVER;
    static const QString PATH_CLIENT;
    static const QString PATH_STUDENTS;
    static const QString PATH_TESTCASES;
    static const QString PATH_SUBMISSIONS;
    static const QString PATH_OUTPUT;

    static const QString FILE_XML_RECORD;
    static const QString FILE_TXT_SCORE;
    static const QString FILE_EXE_MARK;
    static const QString FILE_TXT_WAITING;
    static const QString FILE_XML_PRO;
    static const QString FILE_TXT_LOG;
    static const QString FILE_EXE_MAIN;

    static const QString TAG_XML_ASSIGNMENT;
    static const QString TAG_XML_PROBLEMID;
    static const QString TAG_XML_USERID;
    static const QString TAG_XML_SUBMISSIONID;
    static const QString TAG_XML_PRIORITY;

    static const QString ATTR_XML_ID;
    static const QString ATTR_XML_NUMBER_TESTCASES;
    static const QString ATTR_XML_TIME_LIMIT_EXCEED;

    static const int TIME_LIMIT_COMPILE;
};

#endif // CONFIG_H
