QT += core
QT -= gui
QT += xml

CONFIG += c++11

TARGET = AutoMarkSystem
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    DataStructures/PriorityQueue.cpp \
    Models/student.cpp \
    Models/submission.cpp \
    server.cpp \
    DataStructures/DoubleLinkedList.cpp \
    Models/assignment.cpp \
    DataStructures/Tree.cpp \
    client.cpp \
    Config.cpp \
    utilities.cpp

DISTFILES += \
    .gitignore

HEADERS += \
    DataStructures/PriorityQueue.h \
    Models/student.h \
    Models/submission.h \
    server.h \
    DataStructures/DoubleLinkedList.h \
    Models/assignment.h \
    DataStructures/Tree.h \
    client.h \
    Config.h \
    utilities.h
