#include "server.h"

Server::Server(QMutex *sharedMutex, PriorityQueue<Submission> *sharedQueue)
{
    qDebug() << "===========================  Server is init  ===============================";
    mutex = sharedMutex;
    queue = sharedQueue;

    DirRoot = QDir(Config::ROOT); DirRoot = QDir(DirRoot.absolutePath());
    DirServer = DirRoot; DirServer.cd(Config::PATH_SERVER);
    DirClient = DirRoot; DirClient.cd(Config::PATH_CLIENT);

    qDebug() << DirRoot.path();
    qDebug() << DirClient.path();
    qDebug() << DirServer.path();

     assignmentList = new DoubleLinkedList<Assignment>();

     LoadAssignment();

    qDebug() << "=========================================================================";
}

void Server::LoadAssignment()
{
    qDebug() << "\n";
    qDebug() << "Load Assignment:\n";

    QFile file(DirServer.filePath(Config::FILE_XML_PRO));
    if (!file.open(QFile::ReadOnly)) {
        qWarning("Can not open file assignment: pro.xml");
        return;
    }

    QDomDocument document;
    document.setContent(&file);
    file.close();

    QDomElement root = document.firstChildElement();
    QDomNodeList nodeList = root.elementsByTagName(Config::TAG_XML_ASSIGNMENT);
    for (int i = 0; i < nodeList.count(); i++) {
        QDomNode node = nodeList.at(i);
        if (node.isElement()) {
            QDomElement element = node.toElement();
            QString id = element.attribute(Config::ATTR_XML_ID);
            int numberTestcases = element.attribute(Config::ATTR_XML_NUMBER_TESTCASES).toInt();
            int timeLimitExceed = element.attribute(Config::ATTR_XML_TIME_LIMIT_EXCEED).toInt();

            Assignment ass(id.toStdString().c_str());
            ass.setNumberTestCases(numberTestcases);
            ass.setTimeLimitExceed(timeLimitExceed);
            ass.setPath(DirServer.absoluteFilePath(id));

            mutex->lock();
                assignmentList->InsertLast(ass);
            mutex->unlock();
        }
    }

    qDebug() << "Number assignment: " << assignmentList->Size();
    DoubleLinkedList<Assignment>::Node *pA = assignmentList->GetFirstNode();
    while (pA != nullptr) {
        Assignment ass = pA->data;
        qDebug() << ass.getID() << " - "  << ass.getPath() << "\n"
                 << ass.getNumberTestCases() << " - " << ass.getTimeLimitExceed();
        pA = pA->next;
    }
    qDebug() << "\n";
}

void Server::run()
{
    for (;;) { // tạo vòng lặp vô tận, xử lý hàng đợi submission
        if (!queue->isEmpty()) {
            mutex->lock();
                Submission submission = queue->peak();
            mutex->unlock();

            Assignment assignment("");
            QString assignmentID = submission.getAssignmentID();

            DoubleLinkedList<Assignment>::Node *pH = assignmentList->GetFirstNode();
            while (pH != nullptr) {
                Assignment ass = pH->data;
                if (ass.getID() == assignmentID) {
                    pH->data.addSubmission(submission);
                    assignment.setID(ass.getID());
                    assignment.setNumberTestCases(ass.getNumberTestCases());
                    assignment.setTimeLimitExceed(ass.getTimeLimitExceed());
                    assignment.setPath(ass.getPath());
                    break;
                }
                pH = pH->next;
            }

            qDebug() << "\n\n";
            qDebug() << "=======================  Server  =========================";
            qDebug() << "Submission:";
            qDebug() << "\tPath\t\t:\t" << submission.getPath();
            qDebug() << "\tAssignment\t\t:\t" << submission.getAssignmentID();
            qDebug() << "\tStudent\t\t:\t" << submission.getStudentID();
            qDebug() << "\tPriority\t\t:\t" << submission.getPriority();

            CompileSubmissionSync(submission, assignment);
            RunSubmissionSync(submission, assignment);
            MarkOutputSubmissionSync(submission, assignment);

            qDebug() << "==========================================================";
            qDebug() << "\n\n";
        }
    }
}

void Server::CompileSubmissionSync(Submission submission, Assignment assignment)
{
    qDebug() << "Start Compile:" << submission.getPath();

    QString path = submission.getPath();
    QString program = "g++";
    QStringList arguments;
    arguments << "-std=c++11" << "-Wall" << "main.cpp" << "-o" << Config::FILE_EXE_MAIN;
    QProcess process;
    process.setWorkingDirectory(path);
    process.start(program, arguments);
    bool res = process.waitForFinished(Config::TIME_LIMIT_COMPILE);

    qDebug() << "Compile Finished: \t" << res;
}

void Server::RunSubmissionSync(Submission submission, Assignment assignment)
{
    qDebug() << "Run project:" << submission.getPath();

    QString path = submission.getPath();
    QDir output = QDir(path);
    if (output.cd(Config::PATH_OUTPUT) == false) {
        output.mkdir(Config::PATH_OUTPUT);
        output.cd(Config::PATH_OUTPUT);
    }

    QDir testcase = DirServer;
    testcase.cd(submission.getAssignmentID());
    testcase.cd(Config::PATH_TESTCASES);

    qDebug() << testcase.absolutePath();
    qDebug() << output.absolutePath();

    QString executeFile = QDir(path).filePath(Config::FILE_EXE_MAIN);

    for (int indexTest = 0; indexTest < assignment.getNumberTestCases(); indexTest++) {
        QString inpFile = "INP_" + QString(to_string(indexTest).c_str()) + ".INP";
        QString outFile = "OUT_" + QString(to_string(indexTest).c_str()) + ".OUT";
        inpFile = testcase.absoluteFilePath(inpFile);
        outFile = output.absoluteFilePath(outFile);

        qDebug() << "\tInput File: \t" << inpFile;
        qDebug() << "\tOutput File: \t" << outFile;

        QProcess process;
        process.setStandardInputFile(inpFile);
        process.setStandardOutputFile(outFile);
        process.start(executeFile);
        bool res = process.waitForFinished(assignment.getTimeLimitExceed());

        qDebug() << "\t+Finished test: \t" << indexTest << "\t:\t" << res;
    }
}

void Server::MarkOutputSubmissionSync(Submission submission, Assignment assignment)
{
    qDebug() << "Start Marked project:" << submission.getPath();

    QString path = submission.getPath();
    QDir dirSubmission(path);
    QDir dirAssignment(DirServer.absoluteFilePath(submission.getAssignmentID()));

    QString dirPathSubmission(dirSubmission.absolutePath());
    QString dirPathAssignment(dirAssignment.absolutePath());
    QString executeFile = dirAssignment.absoluteFilePath(Config::FILE_EXE_MARK);
    QString logFile = dirSubmission.absoluteFilePath(Config::FILE_TXT_LOG);

    qDebug() << "\t+Exeute File: \t" << executeFile;
    qDebug() << "\t+Log File: \t" << logFile;
    qDebug() << "\t+Input Dir: \t" << dirPathAssignment;
    qDebug() << "\t+Output Dir: \t" << dirPathSubmission;

    QProcess process;
    process.setStandardOutputFile(logFile);
    process.start(executeFile, QStringList() << dirPathAssignment << dirPathSubmission);
    bool res = process.waitForFinished(assignment.getTimeLimitExceed() * assignment.getNumberTestCases());

    qDebug() << "\t+Finished Mark Project: \t" << res;
}
