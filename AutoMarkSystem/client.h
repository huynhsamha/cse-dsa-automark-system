#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QtXml>
#include <QTextStream>
#include <QProcess>

#include "DataStructures/Tree.h"
#include "DataStructures/PriorityQueue.h"
#include "DataStructures/DoubleLinkedList.h"

#include "Models/student.h"
#include "Models/assignment.h"
#include "Models/submission.h"

#include "Config.h"
#include "utilities.h"

class Client : public QThread
{
    Q_OBJECT
public:
    explicit Client(QMutex *sharedMutex, PriorityQueue<Submission> *sharedQueue);

    // overriding the QThread's run() method
    void run();

private:

    QMutex *mutex; // lấy con trỏ mutex từ hàm main
    PriorityQueue<Submission> *queue; // lấy con trỏ queue từ hàm main

    // các thư mục dùng dùng chính, sử dụng nhiều lần trong project
    QDir DirRoot, DirServer, DirClient;

    // nghe thư mục thay đổi
    QFileSystemWatcher *watcher;

signals:

public slots:
    // khi có signal directory changed từ watcher được emit
    HandleDirectoryChanged(const QString &path);

private:
    bool CheckSubmissionPassed(const QString &path); // Kiểm tra xem submission này cũ hay mới
    bool AddNewSubmissionToQueue(const QString &path); // thử thêm submission này vào queue
    bool ParseFileXMlToSubmission(Submission &submission, const QString &path); // đọc file mô tả xml để tạo submission
};

#endif // CLIENT_H
