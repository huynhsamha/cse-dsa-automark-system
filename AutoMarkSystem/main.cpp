#include <QCoreApplication>

#include "server.h"
#include "client.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // danh sách hàng đợi submission, được truy cập từ 2 luồng server và client
    PriorityQueue<Submission> *queue = new PriorityQueue<Submission>(3000);

    // chặn truy cập đến cùng một tài nguyên từ 2 thread khác nhau
    QMutex *mutex = new QMutex();

    // khởi tạo 2 luồng, truyền các asset dùng chung, mutex dùng để chặn truy cập queue từ thread còn lại
    Server *server = new Server(mutex, queue);
    Client *client = new Client(mutex, queue);

    // gọi hàm run() của 2 thread
    server->start();
    client->start();

    return a.exec();
}
