#ifndef TREE_H
#define TREE_H

template <class T>
class Tree
{
public:
    typedef struct Node {
        T data = "";
        Node *parent = nullptr;
        Node *nextSibling = nullptr;
        Node *previousSibling = nullptr;
        Node *firstChild = nullptr;
        Node *lastChild = nullptr;
        int countChild = 0;
        bool noChild() { return countChild == 0; }
    } Node;

    Node *root;

public:

    Tree() : root(nullptr) {}

    bool AddChild(Node *parent, T data) {
        Node *child = new Node;
        child->data = data;
        child->parent = parent;
        if (parent->lastChild == nullptr) {
            parent->firstChild = parent->lastChild = child;
        }
        else {
            child->previousSibling = parent->lastChild;
            parent->lastChild->nextSibling = child;
            parent->lastChild = child;
        }
        parent->countChild++;
        return true;
    }
};

#endif // TREE_H
