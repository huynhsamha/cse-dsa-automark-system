#ifndef DOUBLELINKEDLIST_H
#define DOUBLELINKEDLIST_H

template <class T>
class DoubleLinkedList
{
public:
    typedef struct Node {
        T data;
        Node *next, *prev;
        Node(T data) : data(data), next(nullptr), prev(nullptr) {}
    } Node;

private:
    Node *firstNode = nullptr;
    Node *lastNode = nullptr;
    int count = 0;

public:
    DoubleLinkedList() : firstNode(nullptr), lastNode(nullptr), count(0) {}
    ~DoubleLinkedList() { clear(); }

    Node *GetFirstNode() { return firstNode; }

    int Size() { return count; }
    int Length() { return count; }
    bool IsEmpty() { return count == 0; }
    void Clear() { clear(); }
    bool InsertFirst(T data) { return insert(data, 0); }
    bool InsertLast(T data) { return insert(data, count); }
    bool Insert(T data, int index) { return insert(data, index); }
    bool RemoveFirst() {return remove(0);}
    bool RemoveLast() {return remove(count-1);}
    bool Remove(int index) { return remove(index); }
    T GetValue(int index) { return GetNode(index)->data; }
    Node *GetNode(int index) { return getPointer(firstNode, index); }

private:
    void clear();
    bool remove(int index);
    bool insert(T data, int index);
    Node *getPointer(Node*head, int index) {
        if (index < 0 || index >= count) throw "Overflow memory";
        for (int i = 0; i < index; i++) {
            if (head != nullptr) head = head->next;
            else throw "Overflow memory";
        }
        return head;
    }
};


template<class T>
bool DoubleLinkedList<T>::insert(T data, int index) {
    Node *newNode = new Node(data);
    if (firstNode == nullptr) { // empty linked list
        if (index != 0) return false; // only insert at index 0
        firstNode = lastNode = newNode;
        ++count;
        return true;
    }
    if (index < 0 || index > count) return false; // only insert in range [0,count]
    if (index == 0) { // insert first
        newNode->next = firstNode;
        firstNode->prev = newNode;
        firstNode = newNode;
        ++count;
        return true;
    }
    if (index == count) { // insert last
        lastNode->next = newNode;
        newNode->prev = lastNode;
        lastNode = newNode;
        ++count;
        return true;
    }
    Node *tmp = firstNode;
    for (int i = 0; i < index; i++) {
        if (tmp != nullptr) tmp = tmp->next;
        else throw "Overflow Memory";
    }
    newNode->next = tmp->next;
    newNode->prev = tmp;
    tmp->next = newNode;
    ++count;
    return true;
}

template<class T>
void DoubleLinkedList<T>::clear() {
    while (firstNode != nullptr) {
        Node *tmp = firstNode;
        firstNode = firstNode->next;
        delete tmp;
    }
    count = 0;
    firstNode = nullptr;
    lastNode = nullptr;
}


template<class T>
bool DoubleLinkedList<T>::remove(int index) {
    if (index < 0 || index >= count) throw "Overflow memory";
    if (firstNode == nullptr) return false;
    if (index == 0) {
        Node *tmp = firstNode;
        firstNode = firstNode->next;
        delete tmp;
        count--;
        return true;
    }
    if (index == count) {
        Node *tmp = lastNode;
        lastNode = lastNode->prev;
        delete tmp;
        count--;
        return true;
    }
    Node *par = nullptr;
    Node *cur = firstNode;
    for (int i = 0; i < index; i++) {
        if (cur == nullptr) throw "Overflow memory";
        par = cur; cur = cur->next;
    }
    par->next = cur->next;
    delete cur;
    cur = nullptr;
    count--;
    return true;
}


#endif // DOUBLELINKEDLIST_H
