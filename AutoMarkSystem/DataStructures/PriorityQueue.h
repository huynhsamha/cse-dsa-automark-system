#ifndef PRIORITY_QUEUE_SHAME
#define PRIORITY_QUEUE_SHAME

#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class PriorityQueue
{
private:
    int _capacity;
    int _lastPosition;
    T *_data;

public:
    PriorityQueue(int capacity);
    ~PriorityQueue() { delete[] _data; }
    int capactity() { return _capacity; }
    int size() { return _lastPosition; }
    bool isEmpty() { return _lastPosition == 0; }
    bool isFull() { return _lastPosition == _capacity; }
    T top() { return _data[0]; }
    bool push(T data);
    bool pop();
    T peak();

protected:
    int leftChild(int i) { return 2 * i + 1; }
    int rightChild(int i) { return 2 * i + 2; }
    int parent(int i) { return (i - 1) / 2; }

    void upHeap(int i);
    void downHeap(int i);
};

template <class T>
PriorityQueue<T>::PriorityQueue(int capacity)
    : _capacity(capacity), _lastPosition(0) {
    _data = new T[capacity];
}

template <class T>
void PriorityQueue<T>::upHeap(int i) {
    while (i > 0) {
        int parent = this->parent(i);
        if (_data[i] > _data[parent]) {
            swap(_data[i], _data[parent]);
            i = parent;
        }
        else {
            break;
        }
    }
}

template <class T>
void PriorityQueue<T>::downHeap(int i) {
    while (i <= _lastPosition) {
        int leftChild = this->leftChild(i);
        int rightChild = this->rightChild(i);
        int largeChild = leftChild;
        if (leftChild > _lastPosition) break;
        if (rightChild <= _lastPosition && _data[leftChild] < _data[rightChild]) {
            largeChild = rightChild;
        }
        if (_data[largeChild] > _data[i]) {
            swap(_data[largeChild], _data[i]);
            i = largeChild;
        }
        else {
            break;
        }
    }
}

template <class T>
bool PriorityQueue<T>::push(T data) {
    if (_lastPosition == _capacity) return false;
    _data[++_lastPosition] = data;
    upHeap(_lastPosition);
    return true;
}

template <class T>
bool PriorityQueue<T>::pop() {
    if (_lastPosition == 0) return false;
    _data[0] = _data[_lastPosition--];
    downHeap(0);
    return true;
}

template <class T>
T PriorityQueue<T>::peak() {
    if (_lastPosition == 0) throw "Queue is empty";
    T res = _data[0];
    _data[0] = _data[_lastPosition--];
    downHeap(0);
    return res;
}

#endif
