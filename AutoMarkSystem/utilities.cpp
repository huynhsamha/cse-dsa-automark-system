#include "utilities.h"

bool Utilities::CopyDirectory(const QString &sourceFilePath, const QString &targetFilePath)
{
    QFileInfo srcFileInfo(sourceFilePath);
    if (srcFileInfo.isDir()) {
        QDir targetDir(targetFilePath);
        targetDir.cdUp();
        if (!targetDir.mkdir(QFileInfo(targetFilePath).fileName())) {
            return false;
        }
        QDir sourceDir(sourceFilePath);
        QStringList fileNames = sourceDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden | QDir::System);
        foreach (const QString &fileName, fileNames) {
            const QString newSrcFilePath = sourceFilePath + QLatin1Char('/') + fileName;
            const QString newTgtFilePath = targetFilePath + QLatin1Char('/') + fileName;
            if (!CopyDirectory(newSrcFilePath, newTgtFilePath)) {
                return false;
            }
        }
    } else {
        if (!QFile::copy(sourceFilePath, targetFilePath)) {
            return false;
        }
    }
    return true;
}
