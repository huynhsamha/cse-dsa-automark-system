'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ejsEngine = require('ejs-locals');
var session = require('express-session');
var helmet = require('helmet');
var chokidar = require('chokidar');
var xmlbuilder = require('xmlbuilder');
var xml2js = require('xml2js');

var config = require('./config');

var index = require('./routes/index');

var app = express();

const mongoose = require('mongoose');

mongoose.connect(config.uriMongo, {
	useMongoClient: true
});

require('./server/models/User');
require('./server/models/Submission');
require('./server/models/Assignment');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('ejs', ejsEngine);

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
	secure: true,
	httpOnly: true,
	secret: config.secret,
	resave: false,
	saveUninitialized: true
}));
app.use(helmet());

require('./server/config/listenChange');

app.use(express.static(path.join(__dirname, 'public')));

var authRoutes = require('./server/routes/auth');
var userRoutes = require('./server/routes/users');
var assignmentRoutes = require('./server/routes/assignments');
var submissionRoutes = require('./server/routes/submissions');

app.use('/', index);
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/assignments', assignmentRoutes);
app.use('/api/submissions', submissionRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error', {
		title: '404 - Page not found',
		usernameUserLogined: req.username
	});
});

module.exports = app;
