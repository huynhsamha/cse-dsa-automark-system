var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto-js');

var UserSchema = new Schema({
	username: { type: String, default: '' },
	password: { type: String, default: '' },
	priority: { type: Number, default: 0 },
	assignments: [{
		id: { type: String, default: '' },
		count: { type: Number, default: 0 },
		score: { type: Number, default: -2 }
	}],
	totalSubmit: { type: Number, default: 0 }
});

UserSchema.methods.hashPassword = function (password) {
	return crypto.AES.encrypt(password, this.username).toString();
};

UserSchema.methods.changePassword = function (oldPassword, newPassword) {
	if (this.authenticate(oldPassword)) {
		this.password = this.hashPassword(newPassword);
		return true;
	}
	return false;
};

UserSchema.methods.authenticate = function (password) {
	var bytes = crypto.AES.decrypt(this.password, this.username);
	var decryptPass = bytes.toString(crypto.enc.Utf8);
	return password === decryptPass;
};

mongoose.model('User', UserSchema);
