var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('./../../config');

var SubmissionSchema = new Schema({
	id: { type: String, default: '' },
	userID: { type: String, default: '' },
	assignmentID: { type: String, default: '' },
	priority: { type: Number, default: 0 },
	timeSubmit: { type: Number, default: new Date().getTime() },
	timeComplete: { type: Number, default: new Date().getTime() },
	source: { type: String, default: '' },
	score: { type: Number, default: -2 } // 0->10, -1: error compile, -2: not complete
});

mongoose.model('Submission', SubmissionSchema);
