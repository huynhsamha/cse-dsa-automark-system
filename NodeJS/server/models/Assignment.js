var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AssignmentSchema = new Schema({
	id: {
		type: String, default: '', index: { unique: true }, uppercase: true
	},
	title: { type: String, default: '' },
	content: { type: String, default: '' },
	numberTestCases: { type: Number, default: 0 },
	timeLimitExceed: { type: Number, default: 0 },
	totalSubmission: { type: Number, default: 0 }
});

mongoose.model('Assignment', AssignmentSchema);
