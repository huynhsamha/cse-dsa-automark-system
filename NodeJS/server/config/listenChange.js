var mongoose = require('mongoose');
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');
var config = require('./../../config');
var responseStatus = require('./../responseStatus');

var chokidar = require('chokidar');
var path = require('path');
var fs = require('fs');
var xml2js = require('xml2js');

var PathDirListenChange = path.join(__dirname, './../../../DSA_DATA/SERVER');

chokidar.watch(
	PathDirListenChange,
	{
		ignored: /(^|[/\\])\../,
		persistent: true,
		ignoreInitial: true
	}
)
	.on('add', filePath => {
		console.log(`File ${filePath} has been added`);
		if (filePath.indexOf('Score.txt') > -1) {
			var dirPath = path.join(filePath, './..');
			console.log(dirPath);
			solveSubmission(dirPath);
		}
	})

	.on('addDir', path => {
		console.log(`Directory ${path} has been added`);

	});


function solveSubmission(dirPath) {
	var proXML = path.join(dirPath, 'pro.xml');
	console.log(proXML);
	fs.readFile(proXML, (err, xml) => {
		if (err) {
			console.log(err); return;
		}
		xml2js.parseString(xml, (err, object) => {
			console.log(object);
			if (!object) return;
			object = object.root;
			if (!object) return;
			var userID = object.userID[0];
			var problemID = object.problemID[0];
			var submissionID = object.submissionID[0];
			console.log(userID, problemID, submissionID);
			var scoreFile = path.join(dirPath, 'Score.txt');
			fs.readFile(scoreFile, 'utf8', (err, file) => {
				if (err) {
					console.log(err); return false;
				}
				console.log(file);
				var num = file.split(/[\n\t' ']/);
				var score = parseFloat(num[0]);
				console.log(score);
				User.findOne({ username: userID }, (err, user) => {
					if (err) { console.log(err); return false; }
					if (!user) { console.log('Not found user ', userID); return false; }
					for (let ass of user.assignments) {
						if (ass.id == problemID) {
							ass.score = Math.max(ass.score, score);
						}
					}
					user.save((err, user) => {
						if (err) { console.log(err); return false; }
					});
				});
				Submission.findOne({ id: submissionID }, (err, sub) => {
					if (err) { console.log(err); return false; }
					if (!sub) { console.log('Not found sub ', submissionID); return false; }
					sub.timeComplete = new Date().getTime();
					sub.score = score;
					sub.save((err, sub) => {
						if (err) { console.log(err); return false; }
					});
				});
			});
		});
	});
}
