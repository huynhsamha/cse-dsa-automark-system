'use strict';

var express = require('express');
var router = express.Router();
var config = require('./../../config');
var authController = require('./../controllers/authController');

router.post('/signin', function (req, res, next) {
	var username = req.body.username || '';
	var password = req.body.password || '';
	authController.signIn(username, password, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			req.session.user = response.user;
			res.send(response);
		}
	});
});

router.post('/signUp', function (req, res, next) {
	var username = req.body.username || '';
	var password = req.body.password || '';
	authController.signUp(username, password, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			req.session.user = response.user;
			res.send(response);
		}
	});
});

router.get('/signOut', function (req, res, next) {
	req.session.destroy((err) => {
		if (err) {
			console.log(err);
		}
		res.send({ status: 200, message: 'OK' });
	});
});

module.exports = router;
