'use strict';

var express = require('express');
var router = express.Router();
var config = require('./../../config');
var submissionController = require('./../controllers/submissionController');

var formidable = require('formidable');
var fs = require('fs');
var path = require('path');

router.post('/submit', (req, res) => {
	var form = new formidable.IncomingForm();
	var uploadDir = path.join(__dirname, './../temp');
	form.uploadDir = uploadDir;
	form.keepExtensions = true;
	form.on('fileBegin', function (name, file) {
		const [fileName, fileExt] = file.name.split('.');
		file.path = path.join(form.uploadDir, `${fileName}_${new Date().getTime()}.${fileExt}`);
	});
	form.parse(req, function (err, fields, files) {
		if (err) {
			console.log(err);
			res.status(500).send({ error: 'Error' });
		} else {
			var userID = fields.userID;
			var problemID = fields.problemID;
			var isFromFile = fields.isFromFile;
			var editorCode = fields.editorCode;
			var filename = files.fileSubmit.name;
			var filepath = files.fileSubmit.path;
			if (isFromFile == 'false') {
				fs.unlinkSync(filepath);
				filename = `${userID}_${problemID}_${new Date().getTime()}.cpp`;
				filepath = path.join(uploadDir, filename);
				fs.writeFileSync(filepath, editorCode);
			}
			submissionController.MakeSubmit(userID, problemID, filename, filepath, (err, response) => {
				if (err) {
					console.log(err); return res.redirect('/profile');
				}
				res.redirect('/profile');
			});
		}
	});
});

router.get('/user', (req, res, next) => {
	var userID = req.query.userID;
	submissionController.getAllSubmissionOfUser(userID, (err, response) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

router.get('/detail', function (req, res, next) {
	var id = req.query.id;
	submissionController.getSubmission(id, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

module.exports = router;
