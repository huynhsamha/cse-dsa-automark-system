'use strict';

var express = require('express');
var router = express.Router();
var config = require('./../../config');
var assignmentController = require('./../controllers/assignmentController');

router.get('/', function (req, res, next) {
	assignmentController.getAllAssignments(function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

router.get('/detail', function (req, res, next) {
	var id = req.query.id;
	assignmentController.getAssignment(id, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

router.post('/', (req, res, next) => {
	var data = {
		id: req.body.id,
		title: req.body.title,
		content: req.body.content,
		numberTestCases: req.body.numberTestCases,
		timeLimitExceed: req.body.timeLimitExceed
	};
	assignmentController.createAssignment(data, (err, response) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

module.exports = router;
