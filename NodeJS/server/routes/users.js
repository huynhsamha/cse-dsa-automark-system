'use strict';

var express = require('express');
var router = express.Router();
var config = require('./../../config');
var userController = require('./../controllers/userController');

router.get('/', function (req, res, next) {
	userController.getAllUsers(function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

router.post('/update', function (req, res, next) {
	var username = req.body.username;
	var priority = req.body.priority;
	userController.updateUser(username, priority, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

router.get('/profile/', function (req, res, next) {
	var username = req.query.username;
	userController.getUser(username, function (err, response) {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(response);
		}
	});
});

module.exports = router;
