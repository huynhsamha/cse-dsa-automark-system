'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');
var config = require('./../../config');
var responseStatus = require('./../responseStatus');

function signIn(username, password, callback) {
	User.findOne({ username: username }, function (err, user) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!user) {
			return callback(responseStatus.Code404({
				errorMessage: 'User not found'
			}));
		}
		if (!user.authenticate(password)) {
			return callback(responseStatus.Code401({
				errorMessage: 'Wrong email or password'
			}));
		}
		user.password = undefined;
		return callback(null, responseStatus.Code200({
			user: user,
			message: 'Sign In successfully'
		}));
	});
}

function signUp(username, password, callback) {
	User.findOne({ username: username }, function (err, user) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (user) {
			return callback(responseStatus.Code401({
				errorMessage: 'User has been existed'
			}));
		}
		var newUser = new User({ username: username });
		newUser.password = newUser.hashPassword(password);
		newUser.save(function (err, user) {
			if (err) {
				console.log(err);
				return callback(responseStatus.Code500());
			} else {
				user.password = undefined;
				return callback(null, responseStatus.Code200({
					user: user,
					message: 'Create user successfully'
				}));
			}
		});
	});
}

module.exports = {
	signIn: signIn,
	signUp: signUp
};
