'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');
var config = require('./../../config');
var responseStatus = require('./../responseStatus');

function getAllAssignments(callback) {
	Assignment.find({ }, function (err, assignments) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!assignments) {
			return callback(responseStatus.Code404({
				errorMessage: 'Assignments not found'
			}));
		}
		return callback(null, responseStatus.Code200({ assignments: assignments }));
	});
}

function getAssignment(id, callback) {
	Assignment.findOne({ id: id }, function (err, assignment) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!assignment) {
			return callback(responseStatus.Code404({
				errorMessage: 'Assignment not found'
			}));
		}
		return callback(null, responseStatus.Code200({ assignment: assignment }));
	});
}

function createAssignment(data, callback) {
	Assignment.findOne({ id: data.id }, (err, ass) => {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (ass) {
			return callback(responseStatus.Code401({ message: 'ID is exist' }));
		}
		var assignment = new Assignment(data);
		assignment.save((err, ass) => {
			if (err) {
				console.log(err);
				return callback(responseStatus.Code500());
			}
			ass.save((err, ass) => {
				if (err) {
					console.log(err);
					return callback(responseStatus.Code500());
				}
				return callback(null, responseStatus.Code200({ assignment: ass }));
			});
		});
	});
}

module.exports = {
	getAllAssignments: getAllAssignments,
	getAssignment: getAssignment,
	createAssignment: createAssignment
};
