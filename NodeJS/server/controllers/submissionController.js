'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');
var config = require('./../../config');
var responseStatus = require('./../responseStatus');

var fs = require('fs');
var path = require('path');
var builder = require('xmlbuilder');

var dirClient = path.join(__dirname, './../../../DSA_DATA/CLIENT');

function MakeSubmit(userID, problemID, filename, filepath, callback) {
	var oldpath = filepath;
	var dirCreate = `${userID}_${problemID}_${new Date().getTime()}`;
	fs.mkdirSync(path.join(dirClient, dirCreate));
	var newpath = path.join(dirClient, dirCreate, 'main.cpp');
	fs.rename(oldpath, newpath, function (err) {
		if (err) {
			console.log(err); return callback(responseStatus.Code500());
		}
		User.findOne({ username: userID }, (err, user) => {
			if (err) {
				console.log(err); return callback(responseStatus.Code500());
			}
			if (!user) {
				console.log('Not User'); return callback(responseStatus.Code404());
			}
			Assignment.findOne({ id: problemID }, (err, ass) => {
				if (err) {
					console.log(err); return callback(responseStatus.Code500());
				}
				fs.readFile(newpath, (err, data) => {
					if (err) {
						console.log(err); return callback(responseStatus.Code500());
					}
					var sub = new Submission({
						userID: userID,
						assignmentID: problemID,
						priority: user.priority,
						source: data,
						timeSubmit: new Date().getTime()
					});
					sub.save(function (err, sub) {
						if (err) {
							console.log(err); return callback(responseStatus.Code500());
						}
						sub.id = sub._id.toString().toUpperCase();
						sub.save(function (err, sub2) {
							if (err) {
								console.log(err); return callback(responseStatus.Code500());
							}
							var fileXML = builder.create({
								root: {
									problemID: {
										'#text': problemID
									},
									userID: {
										'#text': userID
									},
									submissionID: {
										'#text': sub2.id
									},
									priority: {
										'#text': user.priority
									}
								}
							});
							fs.writeFileSync(path.join(dirClient, dirCreate, 'pro.xml'), fileXML);
							user.totalSubmit++;
							var ok = false;
							for (let o of user.assignments) {
								if (o.id == problemID) {
									o.count++; ok = true;
								}
							}
							if (!ok) {
								user.assignments.push({
									id: problemID,
									count: 1
								});
							}
							user.save((err, user) => {
								if (err) {
									console.log(err); return callback(responseStatus.Code500());
								}
								delete user.password;
								ass.totalSubmission++;
								ass.save((err, ass) => {
									if (err) {
										console.log(err); return callback(responseStatus.Code500());
									}
									return callback(responseStatus.Code200({
										submission: sub2,
										assignment: ass,
										user: user
									}));
								});
							});
						});
					});
				});
			});
		});
	});
}

function getAllSubmissionOfUser(userID, callback) {
	Submission.find({ userID: userID }, function (err, submissions) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!submissions) {
			return callback(responseStatus.Code404({
				errorMessage: 'submissions not found'
			}));
		}
		return callback(null, responseStatus.Code200({ submissions: submissions }));
	});
}


function getSubmission(id, callback) {
	Submission.findOne({ id: id }, function (err, submission) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!submission) {
			return callback(responseStatus.Code404({
				errorMessage: 'submission not found'
			}));
		}
		return callback(null, responseStatus.Code200({ submission: submission }));
	});
}

module.exports = {
	MakeSubmit: MakeSubmit,
	getAllSubmissionOfUser: getAllSubmissionOfUser,
	getSubmission: getSubmission
};
