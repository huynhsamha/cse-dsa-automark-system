'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');
var config = require('./../../config');
var responseStatus = require('./../responseStatus');

function getAllUsers(callback) {
	User.find({ }, function (err, users) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!users) {
			return callback(responseStatus.Code404({
				errorMessage: 'Users not found'
			}));
		}
		for (let o of users) delete o.password;
		return callback(null, responseStatus.Code200({
			users: users
		}));
	});
}

function getUser(username, callback) {
	User.findOne({ username: username }, function (err, user) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!user) {
			return callback(responseStatus.Code404({
				errorMessage: 'User not found'
			}));
		}
		delete user.password;
		return callback(null, responseStatus.Code200({ user: user }));
	});
}

function updateUser(username, priority, callback) {
	User.findOne({ username: username }, function (err, user) {
		if (err) {
			console.log(err);
			return callback(responseStatus.Code500());
		}
		if (!user) {
			return callback(responseStatus.Code404({
				errorMessage: 'User not found'
			}));
		}
		user.priority = priority;
		user.save((err, user) => {
			if (err) {
				console.log(err);
				return callback(responseStatus.Code500());
			}
			delete user.password;
			return callback(null, responseStatus.Code200({ user: user }));
		});
	});
}

module.exports = {
	getAllUsers: getAllUsers,
	getUser: getUser,
	updateUser: updateUser
};
