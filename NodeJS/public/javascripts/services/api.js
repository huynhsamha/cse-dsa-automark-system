var app = angular.module('Shama');

app.factory('apiService', function ($http) {

	// console.log('Api Service');

	return {
		signIn: function (username, password) {
			var data = {
				username: username,
				password: password
			};
			return $http.post('/api/auth/signIn', data);
		},
		signUp: function (username, password) {
			var data = {
				username: username,
				password: password
			};
			return $http.post('/api/auth/signUp', data);
		},
		signOut: function () {
			return $http.get('/api/auth/signOut');
		},
		getAllUsers: () => {
			return $http.get('/api/users/');
		},
		getUser: (username) => {
			return $http({
				method: 'GET',
				url: '/api/users/profile',
				params: { username: username }
			});
		},
		updateUser: (username, priority) => {
			var data = {
				username: username,
				priority: priority
			};
			return $http.post('/api/users/update', data);
		},
		getAllAssignments: () => {
			return $http.get('/api/assignments/');
		},
		getAssignment: (id) => {
			return $http({
				method: 'GET',
				url: '/api/assignments/detail',
				params: { id: id }
			});
		},
		createAssignment: (id, title, content, numberTestCases, timeLimitExceed) => {
			var data = {
				id: id.toUpperCase(),
				title: title,
				content: content,
				numberTestCases: numberTestCases,
				timeLimitExceed: timeLimitExceed
			};
			return $http.post('/api/assignments', data);
		},
		getAllSubmissionOfUser: (userID) => {
			return $http({
				method: 'GET',
				url: '/api/submissions/user',
				params: { userID: userID }
			});
		},
		getSubmission: (id) => {
			return $http({
				method: 'GET',
				url: '/api/submissions/detail',
				params: { id: id }
			});
		}
	};
});
