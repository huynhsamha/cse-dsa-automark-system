var app = angular.module('Shama');

app.factory('jqueryConfirm', function ($http) {

	return {
		OneButton_Danger: function (title, content, button, callback) {
			$.confirm({
				theme: 'material',
				title: title,
				content: content,
				closeIcon: true,
				closeIconClass: 'fa fa-close',
				type: 'red',
				buttons: {
					buttonOK: {
						text: 'OK',
						keys: ['enter'],
						btnClass: 'btn-red',
						action: function () { return callback(); }
					}
				}
			});
		},
		OneButton_Success: function (title, content, button, callback) {
			$.confirm({
				theme: 'material',
				title: title,
				content: content,
				closeIcon: true,
				closeIconClass: 'fa fa-close',
				type: 'green',
				buttons: {
					buttonOK: {
						text: 'OK',
						keys: ['enter'],
						btnClass: 'btn-success',
						action: function () { return callback(); }
					}
				}
			});
		}
	};
});
