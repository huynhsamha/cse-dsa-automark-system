(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('headerController', function ($scope, $window, apiService, jqueryConfirm) {

		// console.log('Header Controller');

		var usernameUserLogined = $('#usernameUserLogined').text().trim();

		$scope.username = usernameUserLogined;

		$scope.isUserLogined = () => {
			return usernameUserLogined != '';
		};

		$scope.onClickSignOut = () => {
			jqueryConfirm.OneButton_Danger('Message', 'Are you sure to sign out?', 'OK', () => {
				apiService.signOut().then(res => {
					$window.location.reload();
				});
			});
		};

	});

}());
