(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('signUpController', function ($scope, $window, apiService) {

		// console.log('Sign Up Controller');

		var isFirstTimeClickSignUp = true;

		$scope.username = '';
		$scope.password = '';
		$scope.passwordConfirm = '';

		$scope.error = {
			usernameExist: false,
			server: false
		};

		$scope.NotValidUsername = () => {
			return !isFirstTimeClickSignUp && !($scope.username.match(/^[A-Za-z0-9_-]{3,15}$/));
		};

		$scope.NotValidPassword = () => {
			return !isFirstTimeClickSignUp && $scope.password.length < 5;
		};

		$scope.NotValidConfirmPassword = () => {
			return !isFirstTimeClickSignUp && $scope.password !== $scope.passwordConfirm;
		};

		$scope.onClickSignUp = () => {
			// console.log($scope.username);
			// console.log($scope.password);
			// console.log($scope.passwordConfirm);

			isFirstTimeClickSignUp = false;

			if ($scope.error.usernameExist || $scope.NotValidConfirmPassword()
				|| $scope.NotValidPassword() || $scope.NotValidUsername()) return false;

			apiService.signUp($scope.username, $scope.password).then(res => {
				if (res.data.status === 401) {
					$scope.error.usernameExist = true;
					$scope.error.server = false;
				} else if (res.data.status === 500) {
					$scope.error.server = true;
					$scope.error.usernameExist = false;
				} else if (res.data.status === 200) {
					$window.location.reload();
				}
			});
		};

	});
}());
