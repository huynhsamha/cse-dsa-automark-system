(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('signInController', function ($scope, $window, apiService) {

		// console.log('Sign In Controller');

		$scope.username = '';
		$scope.password = '';

		$scope.error = {
			userNotExist: false,
			wrongPassword: false,
			server: false
		};

		$scope.onClickLogin = () => {
			// console.log($scope.username);
			// console.log($scope.password);
			apiService.signIn($scope.username, $scope.password).then(res => {
				if (res.data.status === 404) {
					$scope.error.userNotExist = true;
					$scope.error.wrongPassword = false;
					$scope.error.server = false;
				} else if (res.data.status === 401) {
					$scope.error.wrongPassword = true;
					$scope.error.userNotExist = false;
					$scope.error.server = false;
				} else if (res.data.status === 500) {
					$scope.error.wrongPassword = false;
					$scope.error.userNotExist = false;
					$scope.error.server = true;
				} else if (res.data.status === 200) {
					$window.location.reload();
				}
			});
		};

	});

}());
