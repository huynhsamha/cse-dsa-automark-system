(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('problemListController', ($scope, $window, apiService, jqueryConfirm) => {

		$scope.listAssignment = [];
		apiService.getAllAssignments().then(res => {
			if (res.data.status == 200) {
				$scope.listAssignment = res.data.assignments;
			}
		});

	});

}());
