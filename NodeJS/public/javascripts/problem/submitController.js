var ace;

(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('problemSubmitController', ($scope, $window, apiService, jqueryConfirm) => {
		var editor = ace.edit('editor');
		ace.require('ace/ext/language_tools');
		ace.require('ace/ext/settings_menu').init(editor);
		editor.setTheme('ace/theme/monokai');
		editor.getSession().setMode('ace/mode/c_cpp');
		editor.setValue(`#include <iostream>

using namespace std;

int main() {

	// your code here

	return 0;
}`);
		editor.gotoLine(10);
		editor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: false
		});

		$scope.onClickSettings = () => {
			editor.showSettingsMenu();
		};

		var isSubmitByFileNotEdit = false;

		var problemID = $('#problemID').text().trim();
		$scope.problemID = problemID;
		var usernameUserLogined = $('#usernameUserLogined').text().trim();
		$scope.userID = usernameUserLogined;
		$scope.apiSubmit = '/api/submissions/submit';
		$scope.ass = {};

		$scope.isUserLogined = () => {
			return usernameUserLogined != '';
		};

		apiService.getAssignment(problemID).then(res => {
			$scope.ass = res.data.assignment;
		});

		$scope.onClickSubmit = () => {
			// console.log(isSubmitByFileNotEdit);
			$('input[name=isFromFile]').val(isSubmitByFileNotEdit);
			$('textarea[name=editorCode]').text(editor.getValue());
			$('#formSubmit').submit();
		};

		$('.input-file').each(function () {
			var $input = $(this),
				$label = $input.next('.js-labelFile'),
				labelVal = $label.html();

			$input.on('change', function (element) {
				var fileName = '';
				if (element.target.value) fileName = element.target.value.split('\\').pop();
				if (fileName) {
					$label.addClass('has-file').find('.js-fileName').html(fileName);
					isSubmitByFileNotEdit = true;
				} else {
					$label.removeClass('has-file').html(labelVal);
					isSubmitByFileNotEdit = false;
				}
			});
		});
	});

}());
