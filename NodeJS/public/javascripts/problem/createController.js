(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('problemCreateController', ($scope, $window, apiService, jqueryConfirm) => {

		var isFirstOnClickCreate = true;

		$scope.id = '';
		$scope.title = '';
		$scope.content = '';
		$scope.numberTestCases = 0;
		$scope.timeLimitExceed = 0;
		$scope.listNumberTestCases = [];
		$scope.listTimeLimit = [];
		for (let i = 1; i < 100; i++) $scope.listNumberTestCases.push(i);
		for (let i = 0.1; i < 5; i += 0.1) $scope.listTimeLimit.push(i.toFixed(1));

		$scope.error = {
			idExist: false,
			server: false
		};

		$scope.NotValidID = () => {
			return !isFirstOnClickCreate && $scope.id == '';
		};

		$scope.NotValidTitle = () => {
			return !isFirstOnClickCreate && $scope.title == '';
		};

		$scope.NotValidContent = () => {
			return !isFirstOnClickCreate && $scope.content == '';
		};

		$scope.NotValidNumberTestcases = () => {
			return !isFirstOnClickCreate && $scope.numberTestCases == 0;
		};

		$scope.NotValidTimeLimit = () => {
			return !isFirstOnClickCreate && $scope.timeLimitExceed == 0;
		};

		$scope.onClickCreateProblem = () => {
			isFirstOnClickCreate = false;

			if ($scope.NotValidID()) return;
			if ($scope.NotValidTitle()) return;
			if ($scope.NotValidContent()) return;
			if ($scope.NotValidNumberTestcases()) return;
			if ($scope.NotValidTimeLimit()) return;

			apiService.createAssignment($scope.id, $scope.title, $scope.content, $scope.numberTestCases, $scope.timeLimitExceed).then(res => {
				if (res.data.status === 500) {
					$scope.error.server = true;
					$scope.error.idExist = false;
				} else if (res.data.status === 200) {
					$scope.error.server = false;
					$scope.error.idExist = false;
					jqueryConfirm.OneButton_Success('Message', 'Create problem successfully!', 'OK', ()=> {
						$window.location.href = '/';
					});
				} else if (res.data.status == 401) {
					$scope.error.idExist = true;
					$scope.error.server = false;
				}
			});
		};
	});

}());
