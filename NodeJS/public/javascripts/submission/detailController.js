var ace;

(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('detailSubmissionController', ($scope, $window, apiService, jqueryConfirm) => {
		var editor = ace.edit('editor');
		ace.require('ace/ext/settings_menu').init(editor);
		editor.setTheme('ace/theme/monokai');
		editor.getSession().setMode('ace/mode/c_cpp');
		editor.setReadOnly(true);

		var submissionID = $('#submissionID').text().trim();
		$scope.submissionID = submissionID;
		var usernameUserLogined = $('#usernameUserLogined').text().trim();
		$scope.userID = usernameUserLogined;
		$scope.ass = {};
		$scope.sub = {};

		$scope.isUserLogined = () => {
			return usernameUserLogined != '';
		};

		apiService.getSubmission(submissionID).then(res => {
			$scope.sub = res.data.submission;
			if ($scope.sub.userID != $scope.userID) {
				$window.location.href = '/';
			}
			editor.setValue($scope.sub.source);
			editor.gotoLine(editor.session.getLength());
			apiService.getAssignment($scope.sub.assignmentID).then(res => {
				$scope.ass = res.data.assignment;
			});
		});
	});

}());
