(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('userListController', ($scope, $window, apiService, jqueryConfirm) => {

		var usernameUserLogined = $('#usernameUserLogined').text().trim();

		$scope.isUserLogined = () => {
			return usernameUserLogined != '';
		};

		// if ($scope.isUserLogined() == false) {
		// 	$window.location.href = '/';
		// }

		$scope.listUser = [];
		apiService.getAllUsers().then(res => {
			if (res.data.status == 200) {
				$scope.listUser = res.data.users;
				updateListUser();
			}
		});
		$scope.indexUserActive = 0;
		$scope.listPriority = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

		$scope.onClickUser = (index) => {
			$scope.indexUserActive = index;
		};

		function updateListUser() {
			for (let o of $scope.listUser) {
				o.updatePriority = o.priority;
			}
		}

		$scope.onClickUpdateUser = (user) => {
			apiService.updateUser(user.username, user.updatePriority).then(res => {
				if (res.data.status == 200) {
					user.priority = user.updatePriority;
					jqueryConfirm.OneButton_Success('Message', `Update user ${user.username} successfully!`, 'OK', () => {

					});
				}
			});
		};
	});

}());
