(function () {
	'use strict';

	var app = angular.module('Shama');

	app.controller('userProfileController', ($scope, $window, apiService, jqueryConfirm) => {

		var usernameUserLogined = $('#usernameUserLogined').text().trim();

		$scope.isUserLogined = () => {
			return usernameUserLogined != '';
		};

		if ($scope.isUserLogined() == false) {
			$window.location.href = '/';
		}

		$scope.listSubmission = [];
		apiService.getAllSubmissionOfUser(usernameUserLogined).then(res => {
			if (res.data.status == 200) {
				$scope.listSubmission = res.data.submissions;
			}
		});

	});

}());
