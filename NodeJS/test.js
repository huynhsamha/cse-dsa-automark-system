'use strict';

const mongoose = require('mongoose');
const config = require('./config');

require('./server/models/User');
require('./server/models/Submission');
require('./server/models/Assignment');

mongoose.connect(config.uriMongo, {
	useMongoClient: true
});
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');


User.find({}, (err, user) => {
	for (let o of user) {
		o.totalSubmit = 0;
		o.assignments = [];
		o.save();
	}
});

Assignment.find({}, (err, user) => {
	for (let o of user) {
		o.totalSubmission = 0;
		o.save();
	}
});

Submission.remove({}, (err) => {
	console.log('remove all');
});
