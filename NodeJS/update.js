'use strict';

const mongoose = require('mongoose');
const config = require('./config');

require('./server/models/User');
require('./server/models/Submission');
require('./server/models/Assignment');

mongoose.connect(config.uriMongo, {
	useMongoClient: true
});
var User = mongoose.model('User');
var Submission = mongoose.model('Submission');
var Assignment = mongoose.model('Assignment');

Submission.find({}, (err, subs) => {
	for (let sub of subs) {
		if (sub.score < 0) {
			sub.score = 0;
			sub.timeComplete = sub.timeSubmit + 10 * 1000;
			sub.save();
		}
	}
});
