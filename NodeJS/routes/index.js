var express = require('express');
var router = express.Router();

router.use((req, res, next) => {
	req.username = (req.session.user) ? req.session.user.username : '';
	next();
});

router.get('/admin', function (req, res, next) {
	if (req.username != 'Admin') return res.redirect('/');
	res.render('user/list', {
		title: 'Manage User',
		usernameUserLogined: req.username
	});
});

router.get('/admin/problem', function (req, res, next) {
	if (req.username != 'Admin') return res.redirect('/');
	res.render('problem/create', {
		title: 'Create Problem',
		usernameUserLogined: req.username,
		id: req.params.id
	});
});

router.use((req, res, next) => {
	if (req.url.includes('api')) return next();
	if (req.username == 'Admin') {
		res.redirect('/admin');
	} else {
		next();
	}
});

/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('problem/list', {
		title: 'Problem',
		usernameUserLogined: req.username
	});
});

router.get('/problem', function (req, res, next) {
	res.render('problem/list', {
		title: 'Problem',
		usernameUserLogined: req.username
	});
});

router.get('/problem/:id', function (req, res, next) {
	res.render('problem/submit', {
		title: 'Problem - ' + req.params.id,
		usernameUserLogined: req.username,
		problemID: req.params.id
	});
});

router.get('/profile', function (req, res, next) {
	res.render('user/profile', {
		title: 'Profile',
		usernameUserLogined: req.username
	});
});

router.get('/user/:id', function (req, res, next) {
	res.render('user/profile', {
		title: 'Shama',
		usernameUserLogined: req.username,
		id: req.params.id
	});
});

router.get('/submission/:id', function (req, res, next) {
	res.render('submission/detail', {
		title: 'Submission - ' + req.params.id,
		usernameUserLogined: req.username,
		submissionID: req.params.id
	});
});

module.exports = router;
