#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

bool testInput(int index, float res, char* outputFile) {

    ifstream fi(outputFile);
    float resI = 0;
    fi >> resI;
    fi.close();

    return (res == resI);
}

int main(int narg, char **args) {

    cout << "Number argment: " << narg << endl;
    cout << "Arguments: \n";
    for (int i = 0; i < narg; i++) {
        cout << args[i] << endl;
    }

    cout << "===============================\n";

    if (narg < 3) {
        cout << "Error: No such necessary folder";
        return 0;
    }

    char* dirProcess = args[1];
    char* dirOutput = args[2];

    cout << "Dir Process: " << dirProcess << endl;
    cout << "Dir Submission: " << dirOutput << endl;

    vector<float> testCaseWeight;
    vector<float> outputRes;
    vector<bool> testCaseCorrect;
    int numTest = 0;
    float res = 0.0;

    char testcase[1000];
    strcpy(testcase, dirProcess);
    strcat(testcase, "/Testcases_Weight.txt");
    
    ifstream fi(testcase);
    // ifstream fi((dirProcess + "/Testcases_Weight.txt").c_str());
    fi >> numTest;
    cout << "Number Testcases: " << numTest << endl;
    cout << "Weight testcases: " << endl;
    for (int i=0;i<numTest;i++) {
        float w; fi >> w; 
        cout << i << " : " << w << endl;
        testCaseWeight.push_back(w);
    }
    fi.close();
    cout << endl;

    cout << "Read output file\n";
    char output[1000];
    strcpy(testcase, dirProcess);
    strcat(testcase, "/OUT.OUT");

    fi.open(testcase);
    for (int i=0;i<numTest;i++) {
        float w; fi >> w; 
        outputRes.push_back(w);
        cout << w * 10 + 5 - 1.9 * 2.5 - 1.0001 + 12.34*12 << " ";
    }
    fi.close();
    cout << endl;

    vector<char*> indexes;
    indexes.push_back("0"); indexes.push_back("1"); indexes.push_back("2");
    indexes.push_back("3"); indexes.push_back("4"); indexes.push_back("5");
    indexes.push_back("6"); indexes.push_back("7"); indexes.push_back("8");
    indexes.push_back("9");

    for (int i = 0; i < (int) testCaseWeight.size(); i++) {
        float w = testCaseWeight[i];
        testCaseCorrect.push_back(false);
        char inputFile[1000], outputFile[1000];
        strcpy(outputFile, dirOutput);  strcat(outputFile, "/OUTPUT/OUT_");
        strcat(outputFile, indexes[i]); strcat(outputFile, ".OUT");
        if (testInput(i, outputRes[i], outputFile)) {
            res += w * 10;
            testCaseCorrect[i] = true;
        }
        cout << "Test " << i << " Finished: " << (testCaseCorrect[i]) << endl;
    }
    cout << "Total Score: \t" << res << endl;

    char score[1000];
    strcpy(score, dirOutput);
    strcat(score, "/Score.txt");
    ofstream fo(score);
    // ofstream fo((dirOutput + "/Score.txt").c_str());
    fo << res << endl;
    for (bool t : testCaseCorrect) fo << t << " ";
    fo << endl;
    fo.close();

    return 0;
}