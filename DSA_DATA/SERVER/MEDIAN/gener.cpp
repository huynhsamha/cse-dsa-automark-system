#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cstdio>

using namespace std;

int n;
int a[1000005];

int main() 
{
  cout << "start" << endl;

  ifstream fs;
  ofstream fo("OUT.OUT");

  for (int ite=0;ite<10;ite++) {
    cout << ite << endl;
    string file = "TESTCASES/INP_" + to_string(ite) + ".INP";
    cout << file << endl;
    fs.open(file.c_str());
    fs >> n;
    cout << n << endl;;
    for (int i=0;i<n;i++) fs >> a[i];
    sort(a, a+n);
    cout << a[(n+1)/2] << endl;
    fo << a[(n+1)/2] << endl;
    fs.close();
  }

  fo.close();

  cout << "end" << endl;

  return 0;

}