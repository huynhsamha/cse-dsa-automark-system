#include <iostream>
#include <algorithm>

using namespace std;

const int N = 1e6+5;
int n;
float a[N];

int main()
{
    cin >> n;
    for (int i=0;i<n;i++) cin >> a[i];

    sort(a, a+n);
    cout << a[(n+1)/2];

    return 0;
}
