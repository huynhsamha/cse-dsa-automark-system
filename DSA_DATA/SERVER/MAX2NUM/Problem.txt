﻿Cho hai số nguyên A, B. Tìm số lớn trong 2 số A, B.

Input:
+ Chứa 2 số nguyên A, B

Output:
+ Giá trị lớn nhất trong 2 số A, B

Giới hạn:
+ -10^15 <= A, B <= 10^15