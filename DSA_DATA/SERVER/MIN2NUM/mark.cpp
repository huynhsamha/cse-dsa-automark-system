#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

bool testInput(int index, char* inputFile, char* outputFile) {
    
    ifstream fi(inputFile);
    long long a, b; fi >> a >> b;
    fi.close();

    long long res = a > b ? b : a;

    fi.open(outputFile);
    long long n; fi >> n;
    fi.close();

    return res == n;
}

int main(int narg, char **args) {

    cout << "Number argment: " << narg << endl;
    cout << "Arguments: \n";
    for (int i = 0; i < narg; i++) {
        cout << args[i] << endl;
    }

    cout << "===============================\n";

    if (narg < 3) {
        cout << "Error: No such necessary folder";
        return 0;
    }

    char* dirProcess = args[1];
    char* dirOutput = args[2];

    cout << "Dir Process: " << dirProcess << endl;
    cout << "Dir Submission: " << dirOutput << endl;

    vector<float> testCaseWeight;
    vector<bool> testCaseCorrect;
    int numTest = 0;
    float res = 0.0;

    char testcase[1000];
    strcpy(testcase, dirProcess);
    strcat(testcase, "/Testcases_Weight.txt");

    ifstream fi(testcase);
    // ifstream fi((dirProcess + "/Testcases_Weight.txt").c_str());
    fi >> numTest;
    cout << "Number Testcases: " << numTest << endl;
    cout << "Weight testcases: " << endl;
    for (int i=0;i<numTest;i++) {
        float w; fi >> w; 
        cout << i << " : " << w << endl;
        testCaseWeight.push_back(w);
    }
    fi.close();
    cout << endl;

    vector<char*> indexes;
    indexes.push_back("0"); indexes.push_back("1"); indexes.push_back("2");
    indexes.push_back("3"); indexes.push_back("4"); indexes.push_back("5");
    indexes.push_back("6"); indexes.push_back("7"); indexes.push_back("8");
    indexes.push_back("9");

    for (int i = 0; i < (int) testCaseWeight.size(); i++) {
        float w = testCaseWeight[i];
        testCaseCorrect.push_back(false);
        char inputFile[1000], outputFile[1000];
        strcpy(inputFile, dirProcess);  strcat(inputFile, "/TESTCASES/INP_");
        strcat(inputFile, indexes[i]);  strcat(inputFile, ".INP");
        strcpy(outputFile, dirOutput);  strcat(outputFile, "/OUTPUT/OUT_");
        strcat(outputFile, indexes[i]); strcat(outputFile, ".OUT");
        // string inputFile = dirProcess + "/INPUT/INP_" + to_string(i) + ".INP";
        // string ouputFile = dirOutput + "/OUT_" + to_string(i) + ".OUT";
        if (testInput(i, inputFile, outputFile)) {
            res += w * 10;
            testCaseCorrect[i] = true;
        }
        cout << "Test " << i << " Finished: " << (testCaseCorrect[i]) << endl;
    }
    cout << "Total Score: \t" << res << endl;

    char score[1000];
    strcpy(score, dirOutput);
    strcat(score, "/Score.txt");
    ofstream fo(score);
    // ofstream fo((dirOutput + "/Score.txt").c_str());
    fo << res << endl;
    for (bool t : testCaseCorrect) fo << t << " ";
    fo << endl;
    fo.close();

    return 0;
}