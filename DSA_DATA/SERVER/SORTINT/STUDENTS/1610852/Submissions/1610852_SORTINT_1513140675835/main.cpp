#include <iostream>
#include <set>

using namespace std;

const int N = 1e6+5;
int n, a[N];
multiset<int> st;

int main()
{
    cin >> n;
    for (int i=0;i<n;i++) cin >> a[i];
    
    for (int i=0;i<n;i++) st.insert(a[i]);

    for (int i=0;i<n;i++) {
        a[i] = *st.begin();
        st.erase(st.begin());
    }

    for (int i=0;i<n;i++) cout << a[i] << " ";
    cout << endl;

    return 0;
}
