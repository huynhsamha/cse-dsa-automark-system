#include <iostream>
#include <algorithm>

using namespace std;

const int N = 1e6+5;
int n;
float a[N];

int main()
{
    cin >> n;
    for (int i=0;i<n;i++) cin >> a[i];

    sort(a, a+n);
    for (int i=0;i<n;i++) cout << a[i] << endl;

    return 0;
}
